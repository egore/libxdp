/*
 * Copyright (c) 2013 Christoph Brill
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.egore911.xdp.util;

import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CalculationUtilTest {

	@Test
	public void testPt2PxVia() {
		assertThat(CalculationUtil.pt2px(100))
				.isCloseTo(CalculationUtil.mm2px(CalculationUtil.pt2mm(100)), Offset.offset(0.001));
		assertThat(CalculationUtil.pt2px(11))
				.isCloseTo(CalculationUtil.mm2px(CalculationUtil.pt2mm(11)), Offset.offset(0.001));
	}

	@Test
	public void testMm2px() {
		assertThat(CalculationUtil.mm2px(Double.parseDouble("8.73")))
				.isCloseTo(33.0027174, Offset.offset(0.001));
	}

	@Test
	public void testParse() {
		assertThat(CalculationUtil.parse("8.73mm"))
				.isCloseTo(8.73, Offset.offset(0.001));
		assertThat(CalculationUtil.parse("-10.513333mm"))
				.isCloseTo(-10.513333, Offset.offset(0.001));
	}

	@Test
	public void testPt2Px() {
		assertThat(CalculationUtil.pt2px(7.5))
				.isCloseTo(10, Offset.offset(0.001));
		assertThat(CalculationUtil.pt2px(6))
				.isCloseTo(8, Offset.offset(0.001));
	}

	@Test
	public void testPx2Pt() {
		assertThat(CalculationUtil.px2pt(10))
				.isCloseTo(7.5, Offset.offset(0.001));
		assertThat(CalculationUtil.px2pt(8))
				.isCloseTo(6, Offset.offset(0.001));
	}

	@Test
	public void testPt2em() {
		assertThat(CalculationUtil.pt2em(6))
				.isCloseTo(0.5, Offset.offset(0.001));
		assertThat(CalculationUtil.pt2em(7.5))
				.isCloseTo(0.625, Offset.offset(0.001));
	}

	@Test
	public void testEm2pt() {
		assertThat(CalculationUtil.em2pt(0.5))
				.isCloseTo(6, Offset.offset(0.001));
		assertThat(CalculationUtil.em2pt(0.625))
				.isCloseTo(7.5, Offset.offset(0.001));
	}
}
